package com.project.apigateway.filter;

import com.project.apigateway.properties.RoutingProperties;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.WebFilter;
import org.springframework.web.server.WebFilterChain;
import reactor.core.publisher.Mono;
import com.project.apigateway.service.JwtService;

@Component
@RequiredArgsConstructor
@Slf4j
public class CustomerFilter implements WebFilter {
    private final JwtService jwtService;
    private final RoutingProperties routingProperties;

    @Override
    @NonNull
    public Mono<Void> filter(ServerWebExchange serverWebExchange, @NonNull WebFilterChain webFilterChain) {
        HttpMethod requestMethod = serverWebExchange.getRequest().getMethod();
        if (HttpMethod.OPTIONS.name().equals(requestMethod.name())) {
            setAccessControlHeaders(serverWebExchange);
            return webFilterChain.filter(serverWebExchange);
        }
        String requestUri = serverWebExchange.getRequest().getPath().toString();
        if (isInternalNotAllowed(requestUri)) {
            return completeUnauthorizedRequest(serverWebExchange);
        }
        if (isPublicEndpointAccess(requestUri)) {
            log.info("Public api: {}", requestUri);
            setAccessControlHeaders(serverWebExchange);
            return webFilterChain.filter(serverWebExchange);
        }
        HttpHeaders headers = serverWebExchange.getRequest().getHeaders();
        String authHeader = headers.getFirst(HttpHeaders.AUTHORIZATION);
        log.info("Private api: {}", requestUri);
        if (StringUtils.isBlank(authHeader)) {
            return completeUnauthorizedRequest(serverWebExchange);
        }
        return jwtService
                .verify(authHeader)
                .flatMap(verify -> {
                    if (verify) {
                        setAccessControlHeaders(serverWebExchange);
                        return webFilterChain.filter(serverWebExchange);
                    }
                    return completeUnauthorizedRequest(serverWebExchange);
                });
    }

    private Mono<Void> completeUnauthorizedRequest(ServerWebExchange serverWebExchange) {
        setAccessControlHeaders(serverWebExchange);
        serverWebExchange.getResponse().setStatusCode(HttpStatus.UNAUTHORIZED);
        return serverWebExchange.getResponse().setComplete();
    }

    private boolean isPublicEndpointAccess(String requestUri) {
        boolean isPermitted = false;
        if (!CollectionUtils.isEmpty(routingProperties.getPublicApis())) {
            for (String healthCheckApi : routingProperties.getPublicApis()) {
                if (requestUri.matches(healthCheckApi)) {
                    isPermitted = true;
                    break;
                }
            }
        }
        return isPermitted;
    }

    private boolean isInternalNotAllowed(String requestUri) {
        boolean isNotAllowed = false;
        if (!CollectionUtils.isEmpty(routingProperties.getInternalApis())) {
            for (String healthCheckApi : routingProperties.getInternalApis()) {
                if (requestUri.matches(healthCheckApi)) {
                    isNotAllowed = true;
                    break;
                }
            }
        }
        return isNotAllowed;
    }
    private void setAccessControlHeaders(ServerWebExchange serverWebExchange) {
        HttpHeaders headers = serverWebExchange.getResponse().getHeaders();
        headers.add(HttpHeaders.ACCESS_CONTROL_ALLOW_ORIGIN,
                serverWebExchange.getRequest().getHeaders().getFirst("Origin"));
        headers.add(HttpHeaders.ACCESS_CONTROL_ALLOW_HEADERS, "withCredentials, Content-Type, Authorization");
        headers.add(HttpHeaders.ACCESS_CONTROL_ALLOW_METHODS, "GET, PUT, POST, PATCH, DELETE, OPTIONS, HEAD");
        headers.add(HttpHeaders.ACCESS_CONTROL_ALLOW_CREDENTIALS, "true");
    }

}
