package com.project.apigateway.service.impl;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSObject;
import com.nimbusds.jose.crypto.RSASSAVerifier;
import com.project.apigateway.dto.response.BaseResponse;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import com.project.apigateway.service.JwtService;

import java.security.KeyFactory;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.X509EncodedKeySpec;
import java.text.ParseException;

@Service
@RequiredArgsConstructor
@Slf4j
public class JwtServiceImpl implements JwtService {
    private long lastKeyFetchingTime = 0L;

    @Value("${jwt.time-refresh-key}")
    private long timePublicKeyRefreshRateLimit;
    private static final String BEARER_PREFIX = "Bearer ";
    private RSAPublicKey rsaPublicKey;
    private final WebClient.Builder webClientBuilder;
    @Value("${jwt.url-fetching-key}")
    private String urlFetchingKey;

    @Override
    public Mono<Boolean> verify(String token) {

        if (token.trim().isEmpty() || !token.startsWith(BEARER_PREFIX)) {
            return Mono.just(Boolean.FALSE);
        }
        token = token.replace("Bearer ", "");
        JWSObject jwsObject;
        try {
            jwsObject = JWSObject.parse(token);
        } catch (Exception e) {
            lastKeyFetchingTime = 0L;
            log.error("Exception when verify token: ", e);
            return Mono.just(Boolean.FALSE);
        }
        return getPublicKey()
                .map(publicKey -> {
                    RSASSAVerifier verifier = new RSASSAVerifier(publicKey);
                    try {
                        return jwsObject.verify(verifier);
                    } catch (Exception e) {
                        lastKeyFetchingTime = 0L;
                        log.error("Exception when verify token: ", e);
                        return Boolean.FALSE;
                    }
                });
    }

    private Mono<RSAPublicKey> getPublicKey() {
        long t = System.currentTimeMillis();
        if (lastKeyFetchingTime == 0L || t - lastKeyFetchingTime >= timePublicKeyRefreshRateLimit) {
            return fetchingKey()
                    .map(publicKey -> {
                        try {
                            byte[] encoded = Base64.decodeBase64(publicKey);
                            KeyFactory kf = KeyFactory.getInstance("RSA");
                            return (RSAPublicKey) kf.generatePublic(new X509EncodedKeySpec(encoded));
                        } catch (Exception e) {
                            log.error("Exception when get public key: ", e);
                        }
                        return rsaPublicKey;
                    });
        }
        return Mono.just(rsaPublicKey);
    }

    private Mono<String> fetchingKey() {
        return webClientBuilder
                .build()
                .get()
                .uri(urlFetchingKey)
                .retrieve()
                .bodyToMono(BaseResponse.class)
                .map(BaseResponse::getData);


    }
}
