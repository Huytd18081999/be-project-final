package com.project.elearning.controller;


import com.project.elearning.dto.response.BaseResponse;
import com.project.elearning.dto.response.CategoriesResponse;
import com.project.elearning.service.CategoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/public-api/categories")
@RequiredArgsConstructor
public class PublicCategoryController {
    private final CategoryService categoryService;

    @GetMapping
    public BaseResponse<List<CategoriesResponse>> getAllCategories() {
        return categoryService.getAllCategory();
    }

}
