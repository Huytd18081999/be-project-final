package com.project.elearning.controller;

import com.project.elearning.dto.response.BaseResponse;
import com.project.elearning.dto.response.CourseResponse;
import com.project.elearning.service.CourseService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/public-api/courses")
@RequiredArgsConstructor
public class PublicCourseController {
    private final CourseService courseService;

    @GetMapping()
    public BaseResponse<List<CourseResponse>> getAllCourse(
            @RequestParam(required = false, name = "id") Long id,
            @RequestParam(required = false, name = "search") String search,
            @RequestParam(required = false, name = "page", defaultValue = "0") Integer page,
            @RequestParam(required = false, name = "size", defaultValue = "50") Integer size
            ) {
        return courseService.getAllCourse(search, id, page, size);
    }
}
