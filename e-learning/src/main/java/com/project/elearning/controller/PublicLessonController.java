package com.project.elearning.controller;

import com.project.elearning.dto.response.BaseResponse;
import com.project.elearning.dto.response.LessonsResponse;
import com.project.elearning.service.LessonService;
import com.project.elearning.utils.ServletUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/public-api/lessons")
@RequiredArgsConstructor
public class PublicLessonController {
    private final LessonService lessonService;
    @GetMapping()
    public BaseResponse<LessonsResponse> getAllLessonsByCourseId(
            @RequestParam(name = "id") Long courseId,
            @RequestParam(name = "lessonId", required = false) Long lessonId) {
        return lessonService.getAllLessonsByCourseId(courseId, lessonId);
    }
}
