package com.project.elearning.controller.secure;

import com.project.elearning.dto.request.CategoryRequest;
import com.project.elearning.dto.response.BaseResponse;
import com.project.elearning.service.CategoryService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/admin-api/categories")
@RequiredArgsConstructor
public class CategoryController {
    private final CategoryService categoryService;
    @PostMapping("")
    public BaseResponse<Object> createCategory(@RequestBody @Valid CategoryRequest categoryRequest) {
        return categoryService.createCategory(categoryRequest);
    }

    @PutMapping("")
    public BaseResponse<Object> updateCategory(@RequestBody @Valid CategoryRequest categoryRequest) {
        return categoryService.updateCategory(categoryRequest);
    }

    @DeleteMapping("/{id}")
    public BaseResponse<Object> deleteCategory(@PathVariable("id") Long categoryId) {
        return categoryService.deleteCategory(categoryId);
    }
}
