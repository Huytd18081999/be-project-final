package com.project.elearning.controller.secure;

import com.project.elearning.dto.request.CourseRequest;
import com.project.elearning.dto.response.BaseResponse;
import com.project.elearning.dto.response.EnrollmentResponse;
import com.project.elearning.service.CourseService;
import com.project.elearning.utils.ServletUtils;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/admin-api/course")
@RequiredArgsConstructor
public class CourseAdminController {
    private final CourseService courseService;

    @PostMapping("")
    public BaseResponse<Object> createCourse(@RequestBody @Valid CourseRequest courseRequest) {
        return courseService.createCourse(courseRequest);
    }

    @PutMapping("")
    public BaseResponse<Object> updateCourse(@RequestBody @Valid CourseRequest courseRequest) {
        return courseService.updateCourse(courseRequest);
    }

    @DeleteMapping("/{id}")
    public BaseResponse<Object> deleteCourse(@PathVariable("id") Long courseId) {
        return courseService.deleteCourse(courseId);
    }
}
