package com.project.elearning.controller.secure;

import com.project.elearning.dto.response.BaseResponse;
import com.project.elearning.dto.response.CourseResponse;
import com.project.elearning.dto.response.EnrollmentResponse;
import com.project.elearning.dto.response.LessonResponse;
import com.project.elearning.service.CourseService;
import com.project.elearning.service.LessonService;
import com.project.elearning.utils.ServletUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/course")
@RequiredArgsConstructor
public class CourseController {
    private final CourseService courseService;
    @GetMapping("/enrollment/{id}")
    public BaseResponse<EnrollmentResponse> enrollmentByCourseId(@PathVariable("id") Long courseId) {
        Long userId = ServletUtils.getUserId();
        return courseService.enrollmentByCourseId(userId, courseId);
    }
    @GetMapping("/my-course")
    public BaseResponse<List<CourseResponse>> getAllMyCourse() {
        Long userId = ServletUtils.getUserId();
        return courseService.getAllMyCourse(userId);
    }
}
