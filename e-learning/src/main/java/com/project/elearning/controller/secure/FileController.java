package com.project.elearning.controller.secure;

import com.project.elearning.dto.response.BaseResponse;
import com.project.elearning.service.CloudinaryService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/admin-api/files")
@RequiredArgsConstructor
public class FileController {
    private final CloudinaryService cloudinaryService;
    @PostMapping("/upload")
    public BaseResponse<String> uploadFile(@RequestParam("file") MultipartFile file) {
            return cloudinaryService.uploadFile(file);
    }
}
