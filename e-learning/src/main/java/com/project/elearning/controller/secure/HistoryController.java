package com.project.elearning.controller.secure;

import com.project.elearning.dto.response.BaseResponse;
import com.project.elearning.dto.response.HistoryResponse;
import com.project.elearning.service.QuestionService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/admin-api/history")
@RequiredArgsConstructor
public class HistoryController {
    private final QuestionService questionService;

    @GetMapping
    public BaseResponse<List<HistoryResponse>> getAllHistorySubmit(@RequestParam(required = false, name = "page", defaultValue = "0") Integer page,
                                                                   @RequestParam(required = false, name = "size", defaultValue = "50") Integer size) {
        return questionService.getAllHistorySubmit(page,size);
    }
}
