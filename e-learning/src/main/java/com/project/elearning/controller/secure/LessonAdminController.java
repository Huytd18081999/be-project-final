package com.project.elearning.controller.secure;

import com.project.elearning.dto.request.LessonRequest;
import com.project.elearning.dto.response.BaseResponse;
import com.project.elearning.service.LessonService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/admin-api/lessons")
@RequiredArgsConstructor
public class LessonAdminController {

    private final LessonService lessonService;

    @PostMapping("")
    public BaseResponse<Object> createLesson(@RequestBody @Valid LessonRequest lessonRequest) {
        return lessonService.createLesson(lessonRequest);
    }

    @PutMapping("")
    public BaseResponse<Object> updateLesson(@RequestBody @Valid LessonRequest lessonRequest) {
        return lessonService.updateLesson(lessonRequest);
    }

    @DeleteMapping("/{id}")
    public BaseResponse<Object> deleteLesson(@PathVariable("id") Long lessonId) {
        return lessonService.deleteLesson(lessonId);
    }

    @GetMapping
    public BaseResponse<Object> getAll(@RequestParam(required = false, name = "courseId") Long id,
                                       @RequestParam(required = false, name = "page", defaultValue = "0") Integer page,
                                       @RequestParam(required = false, name = "size", defaultValue = "50") Integer size) {
        return lessonService.getAll(id, page, size);
    }
}
