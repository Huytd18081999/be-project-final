package com.project.elearning.controller.secure;

import com.project.elearning.dto.response.BaseResponse;
import com.project.elearning.dto.response.LessonResponse;
import com.project.elearning.dto.response.LessonsResponse;
import com.project.elearning.service.LessonService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/lessons")
@RequiredArgsConstructor
public class LessonController {
    private final LessonService lessonService;
    @GetMapping("/{id}")
    public BaseResponse<LessonResponse> getDetailLesson(@PathVariable("id") Long id) {
        return lessonService.getById(id);
    }
}
