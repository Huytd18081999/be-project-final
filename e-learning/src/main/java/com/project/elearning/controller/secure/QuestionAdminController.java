package com.project.elearning.controller.secure;

import com.project.elearning.dto.request.QuizzesRequest;
import com.project.elearning.dto.response.BaseResponse;
import com.project.elearning.dto.response.QuizzesResponse;
import com.project.elearning.service.QuestionService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/admin-api/questions")
@RequiredArgsConstructor
public class QuestionAdminController {
    private final QuestionService questionService;
    @GetMapping()
    public BaseResponse<QuizzesResponse> getQuestionByLessonId(@RequestParam(name = "lessonId") Long lessonId) {
        return questionService.getQuestionByLessonIdForAdmin(lessonId);
    }

    @PostMapping
    public BaseResponse<Object> createQuiz(@RequestBody QuizzesRequest quizzesRequest) {
        return questionService.createQuiz(quizzesRequest);
    }

    @PutMapping
    public BaseResponse<Object> updateQuiz(@RequestBody QuizzesRequest quizzesRequest) {
        return questionService.updateQuiz(quizzesRequest);
    }

    @DeleteMapping("/{id}")
    public BaseResponse<Object> deleteQuiz(@PathVariable("id") Long questionId) {
        return questionService.deleteQuiz(questionId);
    }
}
