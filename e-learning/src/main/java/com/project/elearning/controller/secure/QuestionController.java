package com.project.elearning.controller.secure;

import com.project.elearning.dto.request.AnswerRequest;
import com.project.elearning.dto.response.BaseResponse;
import com.project.elearning.dto.response.QuizzesResponse;
import com.project.elearning.dto.response.SubmitAnswerResponse;
import com.project.elearning.dto.response.TopRankResponse;
import com.project.elearning.entity.QuestionEntity;
import com.project.elearning.service.QuestionService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/questions")
@RequiredArgsConstructor
public class QuestionController {
    private final QuestionService questionService;
    @GetMapping()
    public BaseResponse<QuizzesResponse> getQuestionByLessonId(@RequestParam(name = "lessonId") Long lessonId) {
        return questionService.getQuestionByLessonId(lessonId);
    }

    @PostMapping
    public BaseResponse<SubmitAnswerResponse> submitAnswer(@RequestBody AnswerRequest answerRequest) {
        return questionService.submitAnswer(answerRequest);
    }

    @GetMapping("/top-rank")
    public BaseResponse<TopRankResponse> getTopRankByLessonId(@RequestParam(name = "lessonId") Long lessonId) {
        return questionService.getTopRankByLessonId(lessonId);
    }
}
