package com.project.elearning.dto.request;

import lombok.Getter;

import java.util.List;

@Getter
public class AnswerRequest {
    private Long lessonId;
    private List<ChoicesRequest> choices;
}
