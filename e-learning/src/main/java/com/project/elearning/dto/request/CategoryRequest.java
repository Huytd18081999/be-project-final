package com.project.elearning.dto.request;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class CategoryRequest {

    private Long id;

    @NotBlank(message = "categoryName is required")
    private String name;
}
