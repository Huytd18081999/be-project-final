package com.project.elearning.dto.request;

import lombok.Getter;

@Getter
public class ChoicesRequest {
    private Long questionId;
    private String answer;
}
