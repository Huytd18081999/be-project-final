package com.project.elearning.dto.request;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class LessonRequest {

    private Long lessonId;

    @NotBlank(message = "title is required")
    private String title;

    @NotBlank(message = "description is required")
    private String description;

    @NotBlank(message = "urlVideo is required")
    private String urlVideo;

    @NotBlank(message = "urlContent is required")
    private String urlContent;

    @NotNull(message = "courseId is required")
    private Long courseId;

}
