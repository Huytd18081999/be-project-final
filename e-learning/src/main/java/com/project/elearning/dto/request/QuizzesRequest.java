package com.project.elearning.dto.request;

import lombok.Data;

import java.util.List;

@Data
public class QuizzesRequest {
    private Long questionId;
    private Long lessonId;
    private String question;
    private List<String> choices;
    private String correctAnswer;
}
