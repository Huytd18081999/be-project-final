package com.project.elearning.dto.response;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CategoriesResponse {
    private Long Id;
    private String name;
}
