package com.project.elearning.dto.response;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CorrectAnswerResponse {
    private Long questionId;
    private String question;
    private String correctAnswer;
}
