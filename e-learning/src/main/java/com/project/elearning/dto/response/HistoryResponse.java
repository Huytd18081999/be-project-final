package com.project.elearning.dto.response;

import lombok.Builder;
import lombok.Data;


@Data
@Builder
public class HistoryResponse {
    private Long id;
    private String lesson;
    private String username;
    private Integer correctAnswer;
    private Integer totalQuestion;
}
