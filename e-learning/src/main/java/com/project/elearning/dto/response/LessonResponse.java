package com.project.elearning.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class LessonResponse {
    private Long id;
    private Long courseId;
    private String title;
    private String description;
    private Long orderIndex;
    private String videoUrl;
    private String content;
}
