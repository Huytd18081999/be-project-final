package com.project.elearning.dto.response;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class LessonsAdminResponse {
    private List<LessonResponse> lessons;
    private Long total;
}
