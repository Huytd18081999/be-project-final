package com.project.elearning.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class LessonsResponse {
    @JsonProperty("isEnrollment")
    private boolean isEnrollment;
    private List<LessonResponse> lessons;
    private String title;
    private String description;
}
