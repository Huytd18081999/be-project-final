package com.project.elearning.dto.response;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class QuizzesResponse {
    private String title;
    private Integer totalQuestions;
    private List<QuestionResponse> questions;
}
