package com.project.elearning.dto.response;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class SubmitAnswerResponse {
    private Integer totalCorrect;
    private Integer totalQuestion;
    private List<CorrectAnswerResponse> correctAnswers;
}
