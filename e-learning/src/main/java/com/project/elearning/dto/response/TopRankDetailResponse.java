package com.project.elearning.dto.response;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.SuperBuilder;

import java.sql.Timestamp;

@Data
@SuperBuilder
public class TopRankDetailResponse {
    private String username;
    private Integer correctAnswer;
    private Integer totalQuestion;
    private Timestamp createdAt;
}
