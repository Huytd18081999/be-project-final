package com.project.elearning.dto.response;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class TopRankResponse {
    private YourRankResponse yourRank;
    private List<TopRankDetailResponse> rankByLesson;
}
