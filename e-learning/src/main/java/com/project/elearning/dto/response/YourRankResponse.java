package com.project.elearning.dto.response;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;

@EqualsAndHashCode(callSuper = true)
@Data
@SuperBuilder
public class YourRankResponse extends TopRankDetailResponse{
    private Integer rank;
}
