package com.project.elearning.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.sql.Timestamp;

@Entity
@Table(name = "\"history_point_lesson\"", schema = "public")
@Getter
@Setter
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
public class HistoryPointLessonEntity extends BaseEntity {

    @Column(name = "lesson_id")
    private Long lessonId;

    @Column(name = "username")
    private String username;

    @Column(name = "created_at")
    private Timestamp createdAt;

    @Column(name = "correct_answer")
    private Integer correctAnswer;

    @Column(name = "total_question")
    private Integer totalQuestion;

}
