package com.project.elearning.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.sql.Timestamp;

@Entity
@Table(name = "\"lessons\"", schema = "public")
@Getter
@Setter
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
public class LessonsEntity extends BaseEntity {
    @Column(name = "course_id")
    private Long courseId;
    @Column(name = "title")
    private String title;
    @Column(name = "description")
    private String description;
    @Column(name ="video_url")
    private String videoUrl;
    @Column(name = "content")
    private String content;
    @Column(name = "order_index")
    private Long orderIndex;
    @Column(name = "created_at")
    private Timestamp createdAt;
}
