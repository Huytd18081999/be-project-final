package com.project.elearning.entity;

import jakarta.persistence.Column;
import jakarta.persistence.ElementCollection;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Entity
@Table(name = "\"questions\"", schema = "public")
@Getter
@Setter
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
public class QuestionEntity extends BaseEntity {
    @Column(name = "lesson_id")
    private Long lessonId;

    @Column(name = "question")
    private String question;

    @Column(name = "correct_answers")
    private String correctAnswers;
}
