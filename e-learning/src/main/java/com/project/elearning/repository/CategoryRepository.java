package com.project.elearning.repository;

import com.project.elearning.entity.CategoryEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface CategoryRepository extends JpaRepository<CategoryEntity, Long> {

    @Query(value = "update categories set name = :name where id = :id returning *", nativeQuery = true)
    CategoryEntity updateCategoryById(Long id, String name);
}
