package com.project.elearning.repository;

import com.project.elearning.entity.CourseEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface CourseRepository extends JpaRepository<CourseEntity, Long>, JpaSpecificationExecutor<CourseEntity> {
    List<CourseEntity> findAllByCategoryId(Long id);

    @Query(value = "update courses\n" +
            "set title = :title, description = :description, thumbnail = :thumbnail, category_id = :categoryId\n" +
            "where id = :id \n" +
            "returning *", nativeQuery = true)
    CourseEntity updateCourse(Long id, String title, String description, String thumbnail, Long categoryId);
}

