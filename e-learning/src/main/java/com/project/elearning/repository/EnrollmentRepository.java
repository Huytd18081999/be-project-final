package com.project.elearning.repository;

import com.project.elearning.entity.EnrollmentEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface EnrollmentRepository extends JpaRepository<EnrollmentEntity, Long> {
    Optional<EnrollmentEntity> findByUserIdAndCourseId(Long userId, Long courseId);

    List<EnrollmentEntity> findAllByUserId(Long userId);
}
