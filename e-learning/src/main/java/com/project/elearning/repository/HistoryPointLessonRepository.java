package com.project.elearning.repository;

import com.project.elearning.entity.HistoryPointLessonEntity;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.stream.Collectors;

public interface HistoryPointLessonRepository extends JpaRepository<HistoryPointLessonEntity, Long> {
    @Query(value = "SELECT h.* \n" +
            "FROM history_point_lesson h \n" +
            "INNER JOIN ( \n" +
            "    SELECT username, lesson_id, MAX(correct_answer) AS max_correct_answer \n" +
            "    FROM history_point_lesson \n" +
            "    WHERE lesson_id = :lessonId \n" +
            "    GROUP BY username, lesson_id \n" +
            ") max_h ON h.username = max_h.username AND h.lesson_id = max_h.lesson_id AND h.correct_answer = max_h.max_correct_answer \n" +
            "WHERE h.lesson_id = :lessonId \n" +
            "ORDER BY h.correct_answer DESC, h.created_at", nativeQuery = true)
    List<HistoryPointLessonEntity> findAllByLessonIdOrderByCorrectAnswerDescCreatedAtAsc(Long lessonId);



//    List<HistoryPointLessonEntity> findAllOrderByCreatedAtDesc(PageRequest pageRequest);

    List<HistoryPointLessonEntity> findAllByOrderByCreatedAtDesc(PageRequest pageRequest);
}
