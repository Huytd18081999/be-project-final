package com.project.elearning.repository;

import com.project.elearning.entity.LessonsEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.Collection;
import java.util.List;

public interface LessonRepository extends JpaRepository<LessonsEntity, Long>, JpaSpecificationExecutor<LessonsEntity> {
    List<LessonsEntity> findAllByCourseId(Long id);

    Long countByCourseId(Long courseId);

    @Query(value = "update lessons\n" +
            "set title = :title," +
            " description = :description," +
            " course_id = :courseId," +
            " video_url = :videoUrl," +
            " content = :content\n" +
            "where id = :id\n" +
            "returning *", nativeQuery = true)
    LessonsEntity update(String title, String description, Long courseId, String videoUrl, String content, Long id);

    long deleteByCourseId(Long courseId);

    long deleteByCourseIdIn(Collection<Long> courseIds);
}
