package com.project.elearning.repository;

import com.project.elearning.entity.QuestionChoiceEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface QuestionChoiceRepository extends JpaRepository<QuestionChoiceEntity, Long> {
    List<QuestionChoiceEntity> findAllByQuestionIdIn(List<Long> ids);

    Long deleteByQuestionId(Long questionId);
}
