package com.project.elearning.service;


import com.project.elearning.dto.request.CategoryRequest;
import com.project.elearning.dto.response.BaseResponse;
import com.project.elearning.dto.response.CategoriesResponse;
import com.project.elearning.dto.response.EnrollmentResponse;

import java.util.List;

public interface CategoryService {
    BaseResponse<List<CategoriesResponse>> getAllCategory();

    BaseResponse<Object> createCategory(CategoryRequest categoryRequest);

    BaseResponse<Object> updateCategory(CategoryRequest categoryRequest);

    BaseResponse<Object> deleteCategory(Long categoryId);
}
