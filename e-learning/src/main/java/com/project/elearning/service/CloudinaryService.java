package com.project.elearning.service;

import com.project.elearning.dto.response.BaseResponse;
import org.springframework.web.multipart.MultipartFile;

public interface CloudinaryService {
    BaseResponse<String> uploadFile(MultipartFile file);
}
