package com.project.elearning.service;

import com.project.elearning.dto.request.CourseRequest;
import com.project.elearning.dto.response.BaseResponse;
import com.project.elearning.dto.response.CourseResponse;
import com.project.elearning.dto.response.EnrollmentResponse;

import java.util.List;

public interface CourseService {
    BaseResponse<List<CourseResponse>> getAllCourse(String search, Long id, Integer page, Integer size);

    BaseResponse<EnrollmentResponse> enrollmentByCourseId(Long userId, Long courseId);

    BaseResponse<List<CourseResponse>> getAllMyCourse(Long userId);

    BaseResponse<Object> createCourse(CourseRequest courseRequest);

    BaseResponse<Object> updateCourse(CourseRequest courseRequest);

    BaseResponse<Object> deleteCourse(Long courseId);
}
