package com.project.elearning.service;

import com.project.elearning.dto.request.LessonRequest;
import com.project.elearning.dto.response.BaseResponse;
import com.project.elearning.dto.response.LessonResponse;
import com.project.elearning.dto.response.LessonsResponse;

public interface LessonService {
    BaseResponse<LessonsResponse> getAllLessonsByCourseId(Long id, Long lessonId);

    BaseResponse<LessonResponse> getById(Long id);

    BaseResponse<Object> createLesson(LessonRequest lessonRequest);

    BaseResponse<Object> updateLesson(LessonRequest lessonRequest);

    BaseResponse<Object> deleteLesson(Long lessonId);

    BaseResponse<Object> getAll(Long id, Integer page, Integer size);
}
