package com.project.elearning.service;

import com.project.elearning.dto.request.AnswerRequest;
import com.project.elearning.dto.request.QuizzesRequest;
import com.project.elearning.dto.response.*;

import java.util.List;

public interface QuestionService {
    BaseResponse<QuizzesResponse> getQuestionByLessonId(Long lessonId);

    BaseResponse<SubmitAnswerResponse> submitAnswer(AnswerRequest answerRequest);

    BaseResponse<TopRankResponse> getTopRankByLessonId(Long lessonId);

    BaseResponse<QuizzesResponse> getQuestionByLessonIdForAdmin(Long lessonId);

    BaseResponse<Object> createQuiz(QuizzesRequest quizzesRequest);

    BaseResponse<Object> updateQuiz(QuizzesRequest quizzesRequest);

    BaseResponse<Object> deleteQuiz(Long questionId);

    BaseResponse<List<HistoryResponse>> getAllHistorySubmit(Integer page, Integer size);
}
