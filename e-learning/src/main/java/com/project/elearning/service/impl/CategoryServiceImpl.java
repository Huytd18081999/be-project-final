package com.project.elearning.service.impl;

import com.project.elearning.dto.request.CategoryRequest;
import com.project.elearning.dto.response.BaseResponse;
import com.project.elearning.dto.response.CategoriesResponse;
import com.project.elearning.entity.BaseEntity;
import com.project.elearning.entity.CategoryEntity;
import com.project.elearning.entity.CourseEntity;
import com.project.elearning.exception.BadRequestException;
import com.project.elearning.repository.CategoryRepository;
import com.project.elearning.repository.CourseRepository;
import com.project.elearning.repository.LessonRepository;
import com.project.elearning.service.CategoryService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class CategoryServiceImpl implements CategoryService {
    private final CourseRepository courseRepository;
    private final CategoryRepository categoryRepository;
    private final LessonRepository lessonRepository;
    @Override
    public BaseResponse<List<CategoriesResponse>> getAllCategory() {
        BaseResponse<List<CategoriesResponse>> baseResponse = new BaseResponse<>();
        List<CategoryEntity> categories = categoryRepository.findAll();
        List<CategoriesResponse> res = categories
                .stream()
                .map(category -> CategoriesResponse
                        .builder()
                        .Id(category.getId())
                        .name(category.getName())
                        .build())
                .toList();
        baseResponse.setData(res);
        return baseResponse;
    }

    @Override
    public BaseResponse<Object> createCategory(CategoryRequest categoryRequest) {
        BaseResponse<Object> res = new BaseResponse<>();
        res.setData(categoryRepository.save(CategoryEntity.builder().name(categoryRequest.getName()).build()));
        return res;
    }

    @Override
    public BaseResponse<Object> updateCategory(CategoryRequest categoryRequest) {
        BaseResponse<Object> res = new BaseResponse<>();
        if (categoryRequest.getId() == null) {
            throw new BadRequestException(Collections.singletonList("id is required"));
        }
        res.setData(categoryRepository.updateCategoryById(categoryRequest.getId(), categoryRequest.getName()));
        return res;
    }

    @Override
    @Transactional
    public BaseResponse<Object> deleteCategory(Long categoryId) {
        BaseResponse<Object> res = new BaseResponse<>();
        categoryRepository.deleteById(categoryId);
        List<CourseEntity> courses = courseRepository.findAllByCategoryId(categoryId);
        List<Long> ids = courses.stream().map(CourseEntity::getId).toList();
        courseRepository.deleteAllById(ids);
        lessonRepository.deleteByCourseIdIn(ids);
        return res;
    }
}
