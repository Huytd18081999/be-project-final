package com.project.elearning.service.impl;

import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import com.project.elearning.dto.response.BaseResponse;
import com.project.elearning.service.CloudinaryService;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.Collections;
import java.util.Map;

@Service
@Slf4j
@RequiredArgsConstructor
public class CloudinaryServiceImpl implements CloudinaryService {

    private final Cloudinary cloudinary;
    @SneakyThrows
    @Override
    public BaseResponse<String> uploadFile(MultipartFile file) {
        BaseResponse<String> res = new BaseResponse<>();
        Map<String, String> resourceType;

        String contentType = file.getContentType();
        if (contentType != null && contentType.startsWith("image")) {
            resourceType = ObjectUtils.asMap(
                    "resource_type", "image"
            );
        } else if (contentType != null && contentType.startsWith("video")) {
            resourceType = ObjectUtils.asMap(
                    "resource_type", "video"
            );
        } else {
            resourceType = ObjectUtils.asMap(
                    "resource_type", "raw"
            );
        }
        Map<?, ?> uploadResult = cloudinary.uploader().upload(file.getBytes(), resourceType);
        res.setData((String) uploadResult.get("secure_url"));
        return res;
    }
}
