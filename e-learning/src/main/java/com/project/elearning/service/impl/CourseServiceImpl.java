package com.project.elearning.service.impl;

import com.project.elearning.dto.request.CourseRequest;
import com.project.elearning.dto.response.BaseResponse;
import com.project.elearning.dto.response.CourseResponse;
import com.project.elearning.dto.response.EnrollmentResponse;
import com.project.elearning.entity.CourseEntity;
import com.project.elearning.entity.EnrollmentEntity;
import com.project.elearning.repository.CourseRepository;
import com.project.elearning.repository.EnrollmentRepository;
import com.project.elearning.repository.LessonRepository;
import com.project.elearning.service.CourseService;
import com.project.elearning.specifications.CourseSpecifications;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class CourseServiceImpl implements CourseService {
    private final LessonRepository lessonRepository;
    private final CourseRepository courseRepository;
    private final EnrollmentRepository enrollmentRepository;

    @Override
    public BaseResponse<List<CourseResponse>> getAllCourse(String search, Long id, Integer page, Integer size) {
        BaseResponse<List<CourseResponse>> res = new BaseResponse<>();
        List<CourseResponse> courses = courseRepository
                .findAll(CourseSpecifications.filter(search, id), PageRequest.of(page, size, Sort.by(Sort.Direction.DESC, "createdAt")))
                .stream()
                .map(course -> CourseResponse
                        .builder()
                        .id(course.getId())
                        .title(course.getTitle())
                        .description(course.getDescription())
                        .categoryId(course.getCategoryId())
                        .thumbnail(course.getThumbnail())
                        .build())
                .toList();
        res.setData(courses);
        res.setTotal(courseRepository
                .count(CourseSpecifications.filter(search, id)));
        return res;
    }

    @Override
    public BaseResponse<EnrollmentResponse> enrollmentByCourseId(Long userId, Long courseId) {
        EnrollmentEntity enrollment = enrollmentRepository
                .findByUserIdAndCourseId(userId, courseId).orElse(null);
        if (enrollment == null) {
            enrollment = enrollmentRepository
                    .save(EnrollmentEntity
                            .builder()
                            .userId(userId)
                            .courseId(courseId)
                            .enrollmentDate(new Timestamp(System.currentTimeMillis()))
                            .build());
        }
        BaseResponse<EnrollmentResponse> res = new BaseResponse<>();
        return res;
    }

    @Override
    public BaseResponse<List<CourseResponse>> getAllMyCourse(Long userId) {
        BaseResponse<List<CourseResponse>> res = new BaseResponse<>();
        List<CourseEntity> courses = new ArrayList<>();
        List<EnrollmentEntity> enrollmentEntities = enrollmentRepository.findAllByUserId(userId);
        if (enrollmentEntities.size() > 0) {
            courses = courseRepository.findAllById(enrollmentEntities.stream().map(enrollmentEntity -> enrollmentEntity.getCourseId()).toList());
        }
        List<CourseResponse> courseResponses = courses
                .stream()
                .sorted(Comparator.comparing(CourseEntity::getCreatedAt).reversed())
                .map(course -> CourseResponse
                        .builder()
                        .id(course.getId())
                        .title(course.getTitle())
                        .description(course.getDescription())
                        .categoryId(course.getCategoryId())
                        .thumbnail(course.getThumbnail())
                        .build())
                .toList();
        res.setData(courseResponses);
        return res;
    }

    @Override
    public BaseResponse<Object> createCourse(CourseRequest courseRequest) {
        BaseResponse<Object> res = new BaseResponse<>();
        res.setData(courseRepository
                .save(CourseEntity
                        .builder()
                        .createdAt(new Timestamp(System.currentTimeMillis()))
                        .title(courseRequest.getTitle())
                        .description(courseRequest.getDescription())
                        .thumbnail(courseRequest.getThumbnail())
                        .categoryId(courseRequest.getCategoryId())
                        .build()));
        return res;
    }

    @Override
    public BaseResponse<Object> updateCourse(CourseRequest courseRequest) {
        BaseResponse<Object> res = new BaseResponse<>();
        res.setData(courseRepository
                .updateCourse(courseRequest.getId(), courseRequest.getTitle(), courseRequest.getDescription(), courseRequest.getThumbnail(), courseRequest.getCategoryId()));
        return res;
    }

    @Override
    public BaseResponse<Object> deleteCourse(Long courseId) {
        BaseResponse<Object> res = new BaseResponse<>();
        courseRepository.deleteById(courseId);
        lessonRepository.deleteByCourseId(courseId);
        return res;
    }
}
