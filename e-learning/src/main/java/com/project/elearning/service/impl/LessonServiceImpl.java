package com.project.elearning.service.impl;

import com.project.elearning.constant.ErrorCodes;
import com.project.elearning.dto.request.LessonRequest;
import com.project.elearning.dto.response.BaseResponse;
import com.project.elearning.dto.response.LessonResponse;
import com.project.elearning.dto.response.LessonsAdminResponse;
import com.project.elearning.dto.response.LessonsResponse;
import com.project.elearning.entity.CourseEntity;
import com.project.elearning.entity.EnrollmentEntity;
import com.project.elearning.entity.LessonsEntity;
import com.project.elearning.exception.BadRequestException;
import com.project.elearning.repository.CourseRepository;
import com.project.elearning.repository.EnrollmentRepository;
import com.project.elearning.repository.LessonRepository;
import com.project.elearning.service.LessonService;
import com.project.elearning.specifications.LessonSpecifications;
import com.project.elearning.utils.ServletUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Collections;
import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class LessonServiceImpl implements LessonService {
    private final LessonRepository lessonRepository;
    private final CourseRepository courseRepository;
    private final EnrollmentRepository enrollmentRepository;

    @Override
    public BaseResponse<LessonsResponse> getAllLessonsByCourseId(Long id, Long lessonId) {
        BaseResponse<LessonsResponse> res = new BaseResponse<>();
        Long userId = ServletUtils.getUserId();
        CourseEntity course = courseRepository
                .findById(id)
                .orElseThrow(
                        () -> new BadRequestException(Collections.singletonList(ErrorCodes.LESSON_NOT_FOUND)
                        )
                );
        List<LessonResponse> data;
        if (lessonId == null) {
            data = lessonRepository
                    .findAllByCourseId(id)
                    .stream()
                    .map(entity -> LessonResponse
                            .builder()
                            .id(entity.getId())
                            .courseId(entity.getCourseId())
                            .description(entity.getDescription())
                            .title(entity.getTitle())
                            .orderIndex(entity.getOrderIndex())
                            .build())
                    .toList();
        } else {
            data = lessonRepository
                    .findAllByCourseId(id)
                    .stream()
                    .filter(entity -> entity.getId() == lessonId)
                    .map(entity -> LessonResponse
                            .builder()
                            .id(entity.getId())
                            .courseId(entity.getCourseId())
                            .description(entity.getDescription())
                            .title(entity.getTitle())
                            .orderIndex(entity.getOrderIndex())
                            .build())
                    .toList();
        }

        res.setData(
                LessonsResponse
                        .builder()
                        .lessons(data)
                        .isEnrollment(false)
                        .title(course.getTitle())
                        .description(course.getDescription())
                        .build());
        if (userId != null) {
            EnrollmentEntity enrollment = enrollmentRepository.findByUserIdAndCourseId(userId, id).orElse(null);
            if (enrollment != null) {
                res.getData().setEnrollment(true);
            }
        }
        return res;
    }

    @Override
    public BaseResponse<LessonResponse> getById(Long id) {
        LessonsEntity entity = lessonRepository
                .findById(id)
                .orElseThrow(
                        () -> new BadRequestException(Collections.singletonList(ErrorCodes.LESSON_NOT_FOUND))
                );
        BaseResponse<LessonResponse> res = new BaseResponse<>();
        res.setData(LessonResponse
                .builder()
                .id(entity.getId())
                .courseId(entity.getCourseId())
                .description(entity.getDescription())
                .title(entity.getTitle())
                .videoUrl(entity.getVideoUrl())
                .content(entity.getContent())
                .build());
        Long userId = ServletUtils.getUserId();
        EnrollmentEntity enrollment = enrollmentRepository
                .findByUserIdAndCourseId(userId, entity.getCourseId()).orElse(null);
        if (enrollment == null) {
            enrollmentRepository
                    .save(EnrollmentEntity
                            .builder()
                            .userId(userId)
                            .courseId(entity.getCourseId())
                            .enrollmentDate(new Timestamp(System.currentTimeMillis()))
                            .build());
        }
        return res;
    }

    @Override
    public BaseResponse<Object> createLesson(LessonRequest lessonRequest) {
        BaseResponse<Object> res = new BaseResponse<>();
        res.setData(lessonRepository
                .save(LessonsEntity
                        .builder()
                        .courseId(lessonRequest.getCourseId())
                        .title(lessonRequest.getTitle())
                        .description(lessonRequest.getDescription())
                        .videoUrl(lessonRequest.getUrlVideo())
                        .content(lessonRequest.getUrlContent())
                        .orderIndex(lessonRepository.countByCourseId(lessonRequest.getCourseId()) + 1)
                        .build()));
        return res;
    }

    @Override
    public BaseResponse<Object> updateLesson(LessonRequest lessonRequest) {
        BaseResponse<Object> res = new BaseResponse<>();
        res.setData(lessonRepository
                .update(lessonRequest.getTitle(),
                        lessonRequest.getDescription(),
                        lessonRequest.getCourseId(),
                        lessonRequest.getUrlVideo(),
                        lessonRequest.getUrlContent(),
                        lessonRequest.getLessonId()));
        return res;
    }

    @Override
    public BaseResponse<Object> deleteLesson(Long lessonId) {
        BaseResponse<Object> res = new BaseResponse<>();
        lessonRepository.deleteById(lessonId);
        return res;
    }

    @Override
    public BaseResponse<Object> getAll(Long id, Integer page, Integer size) {
        BaseResponse<Object> res = new BaseResponse<>();
        res.setData(lessonRepository
                        .findAll(LessonSpecifications.filter(id), PageRequest.of(page, size, Sort.by(Sort.Direction.DESC, "createdAt")))
                        .map(lessonsEntity -> LessonResponse
                                .builder()
                                .id(lessonsEntity.getId())
                                .courseId(lessonsEntity.getCourseId())
                                .description(lessonsEntity.getDescription())
                                .title(lessonsEntity.getTitle())
                                .orderIndex(lessonsEntity.getOrderIndex())
                                .build())
                        .stream().toList()

        );
        res.setTotal(lessonRepository.count(LessonSpecifications.filter(id)));
        return res;
    }
}
