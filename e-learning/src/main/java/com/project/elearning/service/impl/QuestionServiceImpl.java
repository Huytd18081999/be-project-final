package com.project.elearning.service.impl;

import com.project.elearning.constant.ErrorCodes;
import com.project.elearning.dto.request.AnswerRequest;
import com.project.elearning.dto.request.ChoicesRequest;
import com.project.elearning.dto.request.QuizzesRequest;
import com.project.elearning.dto.response.*;
import com.project.elearning.entity.HistoryPointLessonEntity;
import com.project.elearning.entity.LessonsEntity;
import com.project.elearning.entity.QuestionChoiceEntity;
import com.project.elearning.entity.QuestionEntity;
import com.project.elearning.exception.BadRequestException;
import com.project.elearning.repository.HistoryPointLessonRepository;
import com.project.elearning.repository.LessonRepository;
import com.project.elearning.repository.QuestionChoiceRepository;
import com.project.elearning.repository.QuestionRepository;
import com.project.elearning.service.QuestionService;
import com.project.elearning.utils.ServletUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
public class QuestionServiceImpl implements QuestionService {

    private final QuestionRepository questionRepository;
    private final LessonRepository lessonRepository;
    private final QuestionChoiceRepository questionChoiceRepository;
    private final HistoryPointLessonRepository historyPointLessonRepository;

    @Override
    public BaseResponse<QuizzesResponse> getQuestionByLessonId(Long lessonId) {
        BaseResponse<QuizzesResponse> res = new BaseResponse<>();
        LessonsEntity lesson = lessonRepository
                .findById(lessonId)
                .orElseThrow(()
                        -> new BadRequestException(Collections.singletonList(ErrorCodes.LESSON_NOT_FOUND)));
        List<QuestionEntity> questionEntities = questionRepository
                .findAllByLessonId(lessonId);
        List<QuestionResponse> questions = questionEntities
                .stream()
                .map(questionEntity -> QuestionResponse
                        .builder()
                        .questionId(questionEntity.getId())
                        .question(questionEntity.getQuestion())
//                        .choices(questionEntity.getChoices())
                        .build())
                .toList();
        List<QuestionChoiceEntity> choices = questionChoiceRepository
                .findAllByQuestionIdIn(questionEntities
                        .stream()
                        .map(entity -> entity.getId())
                        .toList());
        questions = questions
                .stream()
                .map(question -> {
                    question.setChoices(new ArrayList<>());
                    choices.forEach(choice -> {
                        if (question.getQuestionId().equals(choice.getQuestionId())) {
                            question.getChoices().add(choice.getChoice());
                        }
                    });
                    return question;
                })
                .toList();
        res.setData(QuizzesResponse
                .builder()
                .title(lesson.getTitle())
                .questions(questions)
                .totalQuestions(questions.size())
                .build());
        return res;
    }

    @Override
    public BaseResponse<SubmitAnswerResponse> submitAnswer(AnswerRequest answerRequest) {
        BaseResponse<SubmitAnswerResponse> res = new BaseResponse<>();
        List<CorrectAnswerResponse> correctAnswers = questionRepository
                .findAllByLessonId(answerRequest.getLessonId())
                .stream()
                .map(entity -> CorrectAnswerResponse
                        .builder()
                        .correctAnswer(entity.getCorrectAnswers())
                        .question(entity.getQuestion())
                        .questionId(entity.getId())
                        .build())
                .collect(Collectors.toList());
        Integer totalCorrect = 0;
        for (ChoicesRequest answer : answerRequest.getChoices()) {
            for (CorrectAnswerResponse correctAnswer : correctAnswers) {
                if (answer.getQuestionId().equals(correctAnswer.getQuestionId()) && answer.getAnswer().equalsIgnoreCase(correctAnswer.getCorrectAnswer())) {
                    totalCorrect += 1;
                }
            }
        }
        historyPointLessonRepository
                .save(HistoryPointLessonEntity
                        .builder()
                        .lessonId(answerRequest.getLessonId())
                        .totalQuestion(correctAnswers.size())
                        .correctAnswer(totalCorrect)
                        .createdAt(new Timestamp(System.currentTimeMillis()))
                        .username(ServletUtils.getUserName())
                        .build());
        res.setData(SubmitAnswerResponse
                .builder()
                .correctAnswers(correctAnswers)
                .totalCorrect(totalCorrect)
                .totalQuestion(correctAnswers.size())
                .build());
        return res;
    }

    @Override
    public BaseResponse<TopRankResponse> getTopRankByLessonId(Long lessonId) {
        String username = ServletUtils.getUserName();
        BaseResponse<TopRankResponse> res = new BaseResponse<>();

        List<TopRankDetailResponse> topRank = historyPointLessonRepository
                .findAllByLessonIdOrderByCorrectAnswerDescCreatedAtAsc(lessonId)
                .stream()
                .map(historyPointLesson -> TopRankDetailResponse
                        .builder()
                        .username(historyPointLesson.getUsername())
                        .correctAnswer(historyPointLesson.getCorrectAnswer())
                        .totalQuestion(historyPointLesson.getTotalQuestion())
                        .createdAt(historyPointLesson.getCreatedAt())
                        .build())
                .collect(Collectors.toList());
        int size = Math.min(topRank.size(), 10);
        List<TopRankDetailResponse> topTenRank = topRank.subList(0, size);
        YourRankResponse yourRank = null;
        for (int i = 0; i < topRank.size(); ++i) {
            if (username.equalsIgnoreCase(topRank.get(i).getUsername())) {
                yourRank = YourRankResponse
                        .builder()
                        .username(topRank.get(i).getUsername())
                        .correctAnswer(topRank.get(i).getCorrectAnswer())
                        .totalQuestion(topRank.get(i).getTotalQuestion())
                        .createdAt(topRank.get(i).getCreatedAt())
                        .rank(i + 1)
                        .build();
                break;
            }
        }
        res.setData(TopRankResponse
                .builder()
                .rankByLesson(topTenRank)
                .yourRank(yourRank)
                .build());
        return res;
    }

    @Override
    public BaseResponse<QuizzesResponse> getQuestionByLessonIdForAdmin(Long lessonId) {
        BaseResponse<QuizzesResponse> res = new BaseResponse<>();
        LessonsEntity lesson = lessonRepository
                .findById(lessonId)
                .orElseThrow(()
                        -> new BadRequestException(Collections.singletonList(ErrorCodes.LESSON_NOT_FOUND)));
        List<QuestionEntity> questionEntities = questionRepository
                .findAllByLessonId(lessonId);
        List<QuestionResponse> questions = questionEntities
                .stream()
                .map(questionEntity -> QuestionResponse
                        .builder()
                        .questionId(questionEntity.getId())
                        .question(questionEntity.getQuestion())
                        .correctAnswer(questionEntity.getCorrectAnswers())
                        .build())
                .toList();
        List<QuestionChoiceEntity> choices = questionChoiceRepository
                .findAllByQuestionIdIn(questionEntities
                        .stream()
                        .map(entity -> entity.getId())
                        .toList());
        questions = questions
                .stream()
                .map(question -> {
                    question.setChoices(new ArrayList<>());
                    choices.forEach(choice -> {
                        if (question.getQuestionId().equals(choice.getQuestionId())) {
                            question.getChoices().add(choice.getChoice());
                        }
                    });
                    return question;
                })
                .toList();
        res.setData(QuizzesResponse
                .builder()
                .title(lesson.getTitle())
                .questions(questions)
                .totalQuestions(questions.size())
                .build());
        return res;
    }

    @Override
    @Transactional
    public BaseResponse<Object> createQuiz(QuizzesRequest quizzesRequest) {
        BaseResponse<Object> res = new BaseResponse<>();
        QuestionEntity questionEntity = questionRepository
                .save(QuestionEntity
                        .builder()
                        .correctAnswers(quizzesRequest.getCorrectAnswer())
                        .question(quizzesRequest.getQuestion())
                        .lessonId(quizzesRequest.getLessonId())
                        .build());
        quizzesRequest.setQuestionId(questionEntity.getId());
        List<QuestionChoiceEntity> choices = questionChoiceRepository
                .saveAll(quizzesRequest
                        .getChoices()
                        .stream()
                        .map(choiceReq -> QuestionChoiceEntity
                                .builder()
                                .questionId(questionEntity.getId())
                                .choice(choiceReq)
                                .build())
                        .collect(Collectors.toList()));
        res.setData(quizzesRequest);
        return res;
    }

    @Override
    @Transactional
    public BaseResponse<Object> updateQuiz(QuizzesRequest quizzesRequest) {
        BaseResponse<Object> res = new BaseResponse<>();
        QuestionEntity questionEntity = questionRepository
                .save(QuestionEntity
                        .builder()
                        .id(quizzesRequest.getQuestionId())
                        .correctAnswers(quizzesRequest.getCorrectAnswer())
                        .question(quizzesRequest.getQuestion())
                        .lessonId(quizzesRequest.getLessonId())
                        .build());
        questionChoiceRepository.deleteByQuestionId(quizzesRequest.getQuestionId());
        List<QuestionChoiceEntity> choices = questionChoiceRepository
                .saveAll(quizzesRequest
                        .getChoices()
                        .stream()
                        .map(choiceReq -> QuestionChoiceEntity
                                .builder()
                                .questionId(questionEntity.getId())
                                .choice(choiceReq)
                                .build())
                        .collect(Collectors.toList()));
        res.setData(quizzesRequest);
        return res;
    }

    @Override
    @Transactional
    public BaseResponse<Object> deleteQuiz(Long questionId) {
        BaseResponse<Object> res = new BaseResponse<>();
        questionChoiceRepository.deleteByQuestionId(questionId);
        questionRepository.deleteById(questionId);
        return res;
    }

    @Override
    public BaseResponse<List<HistoryResponse>> getAllHistorySubmit(Integer page, Integer size) {
        BaseResponse<List<HistoryResponse>> res = new BaseResponse<>();
        res.setData(new ArrayList<>());
        List<HistoryPointLessonEntity> historyPointLessonEntities = historyPointLessonRepository
                .findAllByOrderByCreatedAtDesc(PageRequest.of(page, size));
        List<LessonsEntity> lessonsEntities = lessonRepository.findAllById(historyPointLessonEntities.stream().map(history -> history.getLessonId()).toList());
        historyPointLessonEntities.forEach(historyPointLessonEntity -> {
            lessonsEntities.forEach(lessonsEntity -> {
                if (historyPointLessonEntity.getLessonId().equals(lessonsEntity.getId())) {
                    res.getData().add(HistoryResponse
                            .builder()
                            .id(historyPointLessonEntity.getId())
                            .lesson(lessonsEntity.getTitle())
                            .username(historyPointLessonEntity.getUsername())
                            .correctAnswer(historyPointLessonEntity.getCorrectAnswer())
                            .totalQuestion(historyPointLessonEntity.getTotalQuestion())
                            .build());
                }
            });
        });
        res.setTotal(historyPointLessonRepository.count());
        return res;
    }
}
