package com.project.elearning.specifications;

import com.project.elearning.entity.LessonsEntity;
import jakarta.persistence.criteria.Predicate;
import org.springframework.data.jpa.domain.Specification;

import java.util.ArrayList;
import java.util.Collection;

public class LessonSpecifications {
    public static Specification<LessonsEntity> filter (Long courseId) {
        return (root, query, criteriaBuilder) -> {
            Collection<Predicate> predicates = new ArrayList<>();
            if (null != courseId) {
                predicates.add(criteriaBuilder.equal(root.get("courseId"), courseId));
            }
            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };
    }
}
