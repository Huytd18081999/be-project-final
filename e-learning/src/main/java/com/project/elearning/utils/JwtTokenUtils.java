package com.project.elearning.utils;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSObject;
import com.nimbusds.jwt.JWTClaimsSet;
import com.project.elearning.constant.TokenField;

import java.text.ParseException;
import java.util.Date;

public class JwtTokenUtils {
    public static Long getUserIdFromToken(String token) throws JOSEException, ParseException {
        JWSObject jwsObject = JWSObject.parse(token);
        JWTClaimsSet claimsSet = JWTClaimsSet.parse(jwsObject.getPayload().toJSONObject());
//        Date expiration = claimsSet.getExpirationTime();
//        if (expiration != null && expiration.before(new Date())) {
//            throw new JOSEException("Token is expired");
//        }

        return (Long) claimsSet.getClaim(TokenField.USER_ID);
    }
    public static String getUsernameFromToken(String token) throws JOSEException, ParseException {
        JWSObject jwsObject = JWSObject.parse(token);
        JWTClaimsSet claimsSet = JWTClaimsSet.parse(jwsObject.getPayload().toJSONObject());
//        Date expiration = claimsSet.getExpirationTime();
//        if (expiration != null && expiration.before(new Date())) {
//            throw new JOSEException("Token is expired");
//        }

        return String.valueOf(claimsSet.getClaim(TokenField.USERNAME));
    }
}
