package com.project.userservice.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.Objects;

@AllArgsConstructor
public enum RoleTypeEnum {
    USER(1),
    ADMIN(2),

    ;

    @Getter
    private Integer code;

    public static RoleTypeEnum get(Integer code) {
        return Objects.requireNonNull(Arrays.stream(RoleTypeEnum.values())
                .filter(s -> s.getCode().equals(code))
                .findFirst()
                .orElse(null));
    }
}