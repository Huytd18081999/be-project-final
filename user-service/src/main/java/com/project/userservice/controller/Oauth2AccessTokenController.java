package com.project.userservice.controller;

import com.project.userservice.dto.request.LoginRequest;
import com.project.userservice.dto.request.RefreshTokenRequest;
import com.project.userservice.dto.response.BaseResponse;
import com.project.userservice.dto.response.Oauth2AccessToken;
import com.project.userservice.service.UserService;
import com.project.userservice.mapper.utils.JwtTokenUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/oauth")
@RequiredArgsConstructor
public class Oauth2AccessTokenController {

    private final JwtTokenUtils jwtTokenUtils;
    private final UserService userService;

    @PostMapping("/token")
    public BaseResponse<Oauth2AccessToken> login(@RequestBody LoginRequest loginRequest) {
        return userService.login(loginRequest);
    }

    @GetMapping("/internal-api/token_key")
    public BaseResponse<String> getTokenKey() {
        BaseResponse<String> response = new BaseResponse<>();
        response.setData(jwtTokenUtils.getPublicKey());
        return response;
    }

    @PostMapping("/refresh_token")
    public BaseResponse<Oauth2AccessToken> refreshToken(@RequestBody RefreshTokenRequest refreshTokenRequest) {
        return userService.refreshToken(refreshTokenRequest);
    }
}