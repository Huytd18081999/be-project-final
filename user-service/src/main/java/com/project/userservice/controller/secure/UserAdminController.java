package com.project.userservice.controller.secure;

import com.project.userservice.dto.response.BaseResponse;
import com.project.userservice.dto.response.UserProfileResponse;
import com.project.userservice.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/admin-api/user")
@RequiredArgsConstructor
public class UserAdminController {
    private final UserService userService;
    @GetMapping()
    public BaseResponse<List<UserProfileResponse>> getAllUser(@RequestParam(required = false, name = "page", defaultValue = "0") Integer page,
                                                              @RequestParam(required = false, name = "size", defaultValue = "50") Integer size) {
        return userService.getAllUser(page, size);
    }
}
