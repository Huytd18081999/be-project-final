package com.project.userservice.controller.secure;

import com.project.userservice.dto.request.RegisterRequest;
import com.project.userservice.dto.response.BaseResponse;
import com.project.userservice.dto.response.UserProfileResponse;
import com.project.userservice.repository.UserRepository;
import com.project.userservice.service.UserService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/user")
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;
    @GetMapping()
    public BaseResponse<UserProfileResponse> getUserInfo() {
        return userService.getUserInfo();
    }
}
