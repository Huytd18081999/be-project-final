package com.project.userservice.dto.request;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.Data;

@Data
public class RegisterRequest {

    @Email(message = "email is invalid")
    @Size(min = 6,  max =255, message = "email must be between {min} and {max} characters long")
    private String email;

    @Size(min = 8, max = 255, message = "email must be between {min} and {max} characters long")
    private String username;

    @Size(min = 8, max = 255, message = "password must be between {min} and {max} characters long")
    @NotBlank(message = "password is require")
    private String password;

    @NotBlank(message = "fullName is require")
    private String fullName;

}
