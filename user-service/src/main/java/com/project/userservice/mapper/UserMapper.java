package com.project.userservice.mapper;

import com.project.userservice.dto.request.RegisterRequest;
import com.project.userservice.dto.response.UserProfileResponse;
import com.project.userservice.entity.User;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component("userMapper")
@RequiredArgsConstructor
public class UserMapper {
    private final PasswordEncoder passwordEncoder;
    public User toEntity(RegisterRequest request) {
        return User
                .builder()
                .username(request.getUsername())
                .password(passwordEncoder.encode(request.getPassword()))
                .fullName(request.getFullName())
                .email(request.getEmail())
                .build();
    }
    public UserProfileResponse toDto(User entity) {
        return UserProfileResponse
                .builder()
                .id(entity.getId())
                .username(entity.getUsername())
                .roleType(entity.getRoleId())
                .build();
    }
}
