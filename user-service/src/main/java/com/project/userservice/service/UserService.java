package com.project.userservice.service;

import com.project.userservice.dto.request.LoginRequest;
import com.project.userservice.dto.request.RefreshTokenRequest;
import com.project.userservice.dto.request.RegisterRequest;
import com.project.userservice.dto.response.BaseResponse;
import com.project.userservice.dto.response.Oauth2AccessToken;
import com.project.userservice.dto.response.UserProfileResponse;

import java.util.List;

public interface UserService {
    BaseResponse<Long> register(RegisterRequest registerRequest);


    BaseResponse<Oauth2AccessToken> login(LoginRequest loginRequest);

    BaseResponse<Oauth2AccessToken> refreshToken(RefreshTokenRequest refreshTokenRequest);

    BaseResponse<UserProfileResponse> getUserInfo();

    BaseResponse<List<UserProfileResponse>> getAllUser(Integer page, Integer size);
}
