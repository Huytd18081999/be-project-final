package com.project.userservice.service.impl;

import com.project.userservice.constant.ErrorCodes;
import com.project.userservice.constant.TokenTypeEnum;
import com.project.userservice.dto.request.LoginRequest;
import com.project.userservice.dto.request.RefreshTokenRequest;
import com.project.userservice.dto.request.RegisterRequest;
import com.project.userservice.dto.response.BaseResponse;
import com.project.userservice.dto.response.Oauth2AccessToken;
import com.project.userservice.dto.response.UserProfileResponse;
import com.project.userservice.entity.User;
import com.project.userservice.exception.BadRequestException;
import com.project.userservice.exception.NotFoundException;
import com.project.userservice.mapper.UserMapper;
import com.project.userservice.repository.UserRepository;
import com.project.userservice.service.UserService;
import com.project.userservice.mapper.utils.JwtTokenUtils;
import com.project.userservice.mapper.utils.ServletUtils;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final JwtTokenUtils jwtTokenUtils;
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final UserMapper userMapper;

    @Override
    public BaseResponse<Long> register(RegisterRequest registerRequest) {
        BaseResponse<Long> response = new BaseResponse<>();
        User user = userRepository.findByUsernameOrEmail(registerRequest.getUsername(), registerRequest.getEmail()).orElse(null);
        if (user != null) {
            throw new BadRequestException(Collections.singletonList(ErrorCodes.USER_ALREADY_EXISTED));
        }
        user = userRepository.save(userMapper.toEntity(registerRequest));
        response.setData(user.getId());
        return response;
    }

    @Override
    public BaseResponse<Oauth2AccessToken> login(LoginRequest loginRequest) {
        User user = userRepository
                .findByUsername(loginRequest.getUsername())
                .orElseThrow(() -> new NotFoundException(Collections.singletonList(ErrorCodes.USER_NOT_FOUND)));

        if (!passwordEncoder.matches(loginRequest.getPassword(), user.getPassword())) {
            throw new BadRequestException(Collections.singletonList(ErrorCodes.INVALID_PASSWORD));
        }

        BaseResponse<Oauth2AccessToken> response = new BaseResponse<>();

        response.setData(jwtTokenUtils.generateOauth2AccessToken(user));

        return response;
    }

    @SneakyThrows
    @Override
    public BaseResponse<Oauth2AccessToken> refreshToken(RefreshTokenRequest refreshTokenRequest) {
        BaseResponse<Oauth2AccessToken> response = new BaseResponse<>();
        if (jwtTokenUtils.getTokenType(refreshTokenRequest.getRefreshToken()).equals(TokenTypeEnum.REFRESH_TOKEN.getCode())) {
            userRepository
                    .findById(jwtTokenUtils.getUserIdFromToken(refreshTokenRequest.getRefreshToken()))
                    .ifPresent(user -> response.setData(jwtTokenUtils.generateOauth2AccessToken(user)));
        }
        return response;
    }


    @SneakyThrows
    public BaseResponse<UserProfileResponse> getUserInfo() {
        Long userId = jwtTokenUtils
                .getUserIdFromToken(ServletUtils.getBearerToken());
        User user = userRepository
                .findById(userId)
                .orElseThrow(()
                        -> new NotFoundException(Collections
                        .singletonList(ErrorCodes.USER_NOT_FOUND)));
        UserProfileResponse userProfileResponse = UserProfileResponse
                .builder()
                .username(user.getUsername())
                .roleType(user.getRoleId())
                .id(userId)
                .build();
        BaseResponse<UserProfileResponse> res = new BaseResponse<>();
        res.setData(userProfileResponse);
        return res;
    }

    @Override
    public BaseResponse<List<UserProfileResponse>> getAllUser(Integer page, Integer size) {
        BaseResponse<List<UserProfileResponse>> res = new BaseResponse<>();
        List<UserProfileResponse> users = userRepository
                .findAll(PageRequest.of(page,size))
                .map(user -> UserProfileResponse
                        .builder()
                        .id(user.getId())
                        .username(user.getUsername())
                        .build())
                .toList();
        res.setData(users);
        res.setTotal(userRepository.count());
        return res;
    }
}
